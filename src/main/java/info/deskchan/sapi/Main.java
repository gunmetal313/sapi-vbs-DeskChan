package info.deskchan.sapi;

import info.deskchan.core.Plugin;
import info.deskchan.core.PluginProxyInterface;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

public class Main implements Plugin{

    private static Main instance;

    private static String SAPI_VBS =  "sapi.vbs";
    private Path SApiVbsPath = null;

    private static PluginProxyInterface pluginProxy;

    public boolean initialize(PluginProxyInterface ppi){
        instance = this;
        pluginProxy = ppi;

        SApiVbsPath = pluginProxy.getPluginDirPath().resolve("sapi").resolve(SAPI_VBS).toFile().toPath();

        pluginProxy.sendMessage("core:add-command", new HashMap(){{
            put("tag", "sapi:say");
        }});

        pluginProxy.addMessageListener("sapi:say", (sender, tag, data) -> {
            String text = "";
            if(data instanceof Map){
                Map<String,Object> mapData = (Map<String,Object>) data;
                if(mapData.containsKey("text"))
                    text=(String) mapData.get("text");
                else if(mapData.containsKey("msgData"))
                    text=(String) mapData.get("msgData");
            } else {
                if(data instanceof String)
                    text=(String) data;
                else text=data.toString();
            }

            final String finalText = text;
            Thread sThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    SApiSay(finalText);
                }
            });
            sThread.start();
            pluginProxy.sendMessage("DeskChan:say#" + "sapi:say", data);
        });

        pluginProxy.sendMessage("core:register-alternatives", Collections.singletonList(new HashMap<String, Object>() {{
            put("srcTag", "DeskChan:say");
            put("dstTag", "sapi:say");
            put("priority", 150);
        }}));

        pluginProxy.sendMessage("core:set-event-link", new HashMap<String, Object>(){{
            put("eventName", "speech:get");
            put("commandName", "sapi:say");
            put("rule", "проговори {msgData:Text}");
        }});
 
        pluginProxy.log("SApi module is ready");
        return true;
    }

    public boolean SApiSay(String message) {
        pluginProxy.log("SApiSay("+ message + ")");
        try {
            byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes());
            String encodesString = new String(encodedBytes);
            Process process =  Runtime.getRuntime().exec("cscript /nologo " + SApiVbsPath.toString()+ " /pipe");
            InputStream in = process.getInputStream();
            Runnable r = () -> {
                String line;
                BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
                try {
                    while ((line = input.readLine()) != null && process.isAlive()) {
                        pluginProxy.log(String.format("sapi.vbs output: %s",line));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            };
            new Thread(r).start();

            OutputStream outputStream = process.getOutputStream();
            PrintStream printStream = new PrintStream(outputStream);
            printStream.print(encodesString);
            printStream.print(System.lineSeparator());
            printStream.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}